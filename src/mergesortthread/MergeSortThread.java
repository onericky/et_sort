/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mergesortthread;

import java.util.Arrays;
import static mergesortthread.Main.mergeSortAlgorithm;

/**
 *
 * @author oneri
 */
public class MergeSortThread extends Thread {
    
    int[] vector;
    int cThread;
    
    /**
     * 
     */
    public MergeSortThread() {
        super();
    }
    
    /**
     * 
     * @param vector
     * @param cThread 
     */
    public MergeSortThread(int[] vector, int cThread) {
        super();
        this.vector = vector;
        this.cThread = cThread;
        start();
    }

    /**
     * 
     */
    public void run() {
        parallelMergeSort(vector, cThread);
    }

    /**
     * 
     * @param vec
     * @param tCounts 
     */
    public void parallelMergeSort(int[] vec, int tCounts) {
        // divide vector in two
        int[] left  = Arrays.copyOfRange(vec, 0, vec.length / 2);
        int[] right = Arrays.copyOfRange(vec, vec.length / 2, vec.length);

        // create new threads
        // * function Main Class
        mergeSortAlgorithm(left, tCounts);
        mergeSortAlgorithm(right, tCounts);

        // join halves
        joinHalves(left, right, vec);
    }
    
    /**
     * Divide the array given in two and recursively order each half
     * then join the halves into one
     * 
     * @param vec 
     */
    public static void mergeSort(int[] vec) {
        if(vec.length >= 2) {
            // divide array in two
            int[] left  = Arrays.copyOfRange(vec, 0, vec.length / 2);
            int[] right = Arrays.copyOfRange(vec, vec.length / 2, vec.length);

            // sort each half
            mergeSort(left);
            mergeSort(right);

            // join halves
            joinHalves(left, right, vec);
        }
    }

    /**
     * Join left and right array into one
     * 
     * @param left
     * @param right
     * @param a 
     */
    public static void joinHalves(int[] left, int[] right, int[] a) {
        int i1 = 0;
        int i2 = 0;
        for (int i = 0; i < a.length; i++) {
            if (i2 >= right.length || (i1 < left.length && left[i1] < right[i2])) {
                a[i] = left[i1];
                i1++;
            } else {
                a[i] = right[i2];
                i2++;
            }
        }
    }
}
