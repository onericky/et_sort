/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mergesortthread;

/**
 *
 * @author oneri
 */
public class ThreadRun implements Runnable {
    private int[] vector;
    private int threads;

    public ThreadRun(int[] vector, int threads) {
        this.vector = vector;
        this.threads = threads;
    }

    public void run() {
        Main.mergeSortAlgorithm(vector, threads);
    }
}
