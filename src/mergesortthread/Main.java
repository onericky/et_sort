/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mergesortthread;

import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

//import static mergesortthread.MergeSortThread.joinHalves;
//import static mergesortthread.MergeSortThread.mergeSort;

/**
 *
 * @author oneri
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        System.out.print("Ruta de archivo a ordenar: \n");
        String path = sc.nextLine();
        
        int[] vector = readFile(path);
        
        Date tiempoi = new Date();
        long t1 = tiempoi.getTime();
        
        // create threads
        mergeSortAlgorithm(vector);
        
        Date tiempof = new Date();
        long t2 = tiempof.getTime();

        System.out.println("Tiempo inicio: " + t1 + " ms");
        System.out.println("Tiempo fin: " + t2 + " ms");
        System.out.println("Tiempo total: " + (t2 - t1) + " ms");
        
        saveFile(vector, path);
    }
    
    /**
     * 
     * @param vector 
     */
    public static void mergeSortAlgorithm(int[] vector) {
        int threads = 16;
        mergeSortAlgorithm(vector, threads);
    }
    
    /**
     * 
     * @param vector
     * @param threads 
     */
    public static void mergeSortAlgorithm(int[] vector, int threads) {
        if(threads <= 1) {
            mergeSort(vector);
        } else if(vector.length >= 2) {
           // divide vector in two
            int[] left = Arrays.copyOfRange(vector, 0, vector.length / 2);
            int[] right = Arrays.copyOfRange(vector, vector.length / 2, vector.length);

            Thread leftThread = new Thread(new ThreadRun(left,  threads / 2));
            Thread rightThread = new Thread(new ThreadRun(right, threads / 2));
            
            leftThread.start();
            rightThread.start();

            try {
                leftThread.join();
                rightThread.join();
            } catch(InterruptedException e) {
                System.out.print(e.getMessage());
            }

            // join halves
            joinHalves(left, right, vector);
        }
    }
    
    /**
     * Join left and right array into one
     * 
     * @param left
     * @param right
     * @param vector 
     */
    public static void joinHalves(int[] left, int[] right, int[] vector) {
        int a = 0, b = 0;
        for(int i = 0; i < vector.length; i++) {
            if(b >= right.length || (a < left.length && left[a] < right[b])) {
                vector[i] = left[a];
                a ++;
            } else {
                vector[i] = right[b];
                b ++;
            }
        }
    }
    
    /**
     * Divide the array given in two and recursively order each half
     * then join the halves into one
     * 
     * @param vector 
     */
    public static void mergeSort(int[] vector) {
        if(vector.length >= 2) {
            // divide array in two
            int[] left = Arrays.copyOfRange(vector, 0, vector.length / 2);
            int[] right = Arrays.copyOfRange(vector, vector.length / 2, vector.length);

            // sort each half
            mergeSort(left);
            mergeSort(right);

            // join halves
            joinHalves(left, right, vector);
        }
    }
    

    /**
     * 
     * @param vector
     * @param threads 
     */
    public static void mergeSortAlgorithm2(int[] vec, int threads) {
        //System.out.println(" **************** " + thread);
        if(threads <= 1) {
            // * function MergeSort Class
            mergeSort(vec);
        } else if(vec.length >= 2) {
            // divide vector in two
            int[] left  = Arrays.copyOfRange(vec, 0, vec.length / 2);
            int[] right = Arrays.copyOfRange(vec, vec.length / 2, vec.length);

            // create a thread for each half
            MergeSortThread mst1 = new MergeSortThread(left, threads);
            MergeSortThread mst2 = new MergeSortThread(right, threads);
            
            try {
                mst1.join();
                mst2.join();
            } catch(InterruptedException e) {
                System.out.print(e.getMessage());
            }

            // join halves
            // * function MergeSortThread Class
            joinHalves(left, right, vec);
        }
    }
    
    /**
     * Read CSV file and return int[]
     * 
     * @param path
     * @return 
     */
    public static int[] readFile(String path) {
        File file = new File(path);
        List<String> list = new ArrayList<>();
        Scanner inputStream;
        
        try {
             inputStream = new Scanner(file);

            while(inputStream.hasNext()){
                String line = inputStream.next();
                String[] values = line.split(",");
                
                for(int i = 0; i< values.length; i++) {
                    if(values[i].trim() == "") continue;
                    list.add(values[i].trim());
                }
            }

            inputStream.close();
        } catch(FileNotFoundException e) {
             System.out.print(e.getMessage());
        }
        
        int[] numbers = new int[list.size()];
        for(int i = 0; i< list.size(); i++) {
            numbers[i] = Integer.parseInt(list.get(i));
        }
        
        return numbers;
    }
    
    /**
     * Save CSV file
     * 
     * @param result
     * @param path 
     */
    public static void saveFile(int[] result, String path) {
        Path path2 = Paths.get(path);
        Path fileName = path2.getFileName();

        String parentPath = "";
        if(path2.getParent() != null) {
            parentPath = path2.getParent().toString() + "\\";
        }
                
        try {
            FileWriter csvWriter = new FileWriter(parentPath + (fileName.toString()).replaceFirst("[.][^.]+$", "") + "_ord.csv");
            for(int i = 0; i< result.length; i++) {
                csvWriter.append(String.valueOf(result[i]).concat(","));
            }
            csvWriter.flush();
            csvWriter.close();
        } catch(IOException e) {
             System.out.print(e.getMessage());
        }
    }
}
